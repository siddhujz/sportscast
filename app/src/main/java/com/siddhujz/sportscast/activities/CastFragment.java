package com.siddhujz.sportscast.activities;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.siddhujz.sportscast.CastListAdapter;
import com.siddhujz.sportscast.R;
import com.siddhujz.sportscast.models.CuratedContent;

import java.util.List;

/**
 * Created by siddhartha on 10/3/15.
 */
/**
 * A cast fragment containing a simple view.
 */
public class CastFragment extends ListFragment {

    private static final String TAG = "CastFragment";

    // curatedContentArray(data) object we want to retain
    public CuratedContent[] mCuratedContentArray;

    public CastFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedState) {
        Log.i(TAG, getClass().getSimpleName() + ":entered onActivityCreated()");
        super.onActivityCreated(savedState);

        // Set the list adapter for the ListView
        // Discussed in more detail in the user interface classes lesson
        Log.i(TAG, getClass().getSimpleName() + "values = <<" + MainActivity.mCuratedContentArray + ">>");
        setListAdapter(new CastListAdapter(getActivity(),
                R.layout.curated_content_item, this.mCuratedContentArray));

        // Set the list choice mode to allow only one selection at a time
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    public CuratedContent[] getmCuratedContentArray() {
        return mCuratedContentArray;
    }

    public void setmCuratedContentArray(CuratedContent[] mCuratedContentArray) {
        this.mCuratedContentArray = mCuratedContentArray;
    }
}
