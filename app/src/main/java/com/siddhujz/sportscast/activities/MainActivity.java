package com.siddhujz.sportscast.activities;

import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.siddhujz.sportscast.CuratedContentAsyncTask;
import com.siddhujz.sportscast.R;
import com.siddhujz.sportscast.models.CuratedContent;
import com.siddhujz.sportscast.utilities.ConnectionDetector;
import com.siddhujz.sportscast.utilities.Utilities;
import com.siddhujz.sportscast.utilities.YahooPipeCast;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    boolean isInternetConnected = false;
    ConnectionDetector connectionDetector;

    public static CuratedContent[] mCuratedContentArray;
    //CuratedContentAsyncTask curatedContentAsyncTask = new CuratedContentAsyncTask();

    private CastFragment castFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, getClass().getSimpleName() + ":entered onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Check if Internet is connected
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetConnected = connectionDetector.isConnectingToInternet();
        Log.i(TAG, getClass().getSimpleName() + ":isInternetConnected = " + isInternetConnected);

        //If Internet is connected, execute the curatedContentAsyncTask to Get the curatedContentList nad populate the mCuratedContentArray
        if (isInternetConnected) {
            if (savedInstanceState == null) {
                //Execute curatedContentAsyncTask to Get the curatedContentList nad populate the mCuratedContentArray
                new CuratedContentAsyncTask(this, "").execute();
            } else {
                FragmentManager fm = getFragmentManager();
                castFragment = (CastFragment) fm.findFragmentById(R.id.container);
                if (castFragment != null) {
                    // the data is available in castFragment.getmCuratedContentArray
                    mCuratedContentArray = castFragment.getmCuratedContentArray();
                }
            }
        } else {
            Log.i(TAG, getClass().getSimpleName() + ":No Network Connection.");
        }
    }

    public void getResult(List<CuratedContent> curatedContentList) throws Exception {
        if (castFragment == null) {
            castFragment = new CastFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, castFragment)
                    .commit();
        }
        mCuratedContentArray = curatedContentList.toArray(new CuratedContent[curatedContentList.size()]);
        // load the data from the web
        castFragment.setmCuratedContentArray(mCuratedContentArray);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
