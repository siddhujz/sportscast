package com.siddhujz.sportscast;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.siddhujz.sportscast.activities.WebCastActivity;
import com.siddhujz.sportscast.models.CuratedContent;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by siddhartha on 12/3/15.
 */
public class CastListAdapter extends ArrayAdapter<CuratedContent> {

    private static final String TAG = "CastListAdapter";

    private int resource;
    private LayoutInflater inflater;
    private Context context;

    public CastListAdapter(Context ctx, int resourceId, CuratedContent[] objects) {

        super(ctx, resourceId, objects);
        resource = resourceId;
        inflater = LayoutInflater.from(ctx);
        context = ctx;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

		/* create a new view of my layout and inflate it in the row */
        convertView = (RelativeLayout) inflater.inflate(resource, null);

        View row = convertView;

		/* Extract the CuratedContent object to show */
        final CuratedContent curatedContent = getItem(position);

		/* Take the TextView from layout and set the castTitle */
        TextView castTitle = (TextView) convertView.findViewById(R.id.castTitle);
        castTitle.setText(curatedContent.getTitle());

		/* Take the TextView from layout and set the castDesc */
        TextView castDesc = (TextView) convertView.findViewById(R.id.castDesc);
        castDesc.setText(curatedContent.getDescription());

		/* Take the ImageView from layout and set the castImage using AsychTask */
        ImageView castImage = (ImageView) convertView.findViewById(R.id.castImage);
        //new ImageLoadTask(curatedContent.getImage_url(), castImage).execute();
        Picasso.with(context)
                .load(curatedContent.getImage_url())
                .fit()
                .into(castImage);

        /* On click event for a row in the CastList */
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "ROW PRESSED");
                Intent myIntent = new Intent(view.getContext(), WebCastActivity.class);
                myIntent.putExtra("sportscast_url", curatedContent.getUrl()); //Optional parameters
                view.getContext().startActivity(myIntent);
            }
        });

        return convertView;
    }
}