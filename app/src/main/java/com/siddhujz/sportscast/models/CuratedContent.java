package com.siddhujz.sportscast.models;

/**
 * Created by siddhartha on 11/3/15.
 */
public class CuratedContent {
    private String id;
    private String datePublished;
    private String source;
    private String mediaTypeRank;
    private String title;
    private String url;
    private String description;
    private String image_url;
    private String author;
    private long date_created;
    private long likes;
    private long dislikes;
    private long shares;
    private String date_created_String;

    public CuratedContent() {}

    public CuratedContent(String id, String datePublished, String source, String mediaTypeRank, String title, String url, String description, String image_url, String author, long date_created, long likes, long dislikes, long shares, String date_created_String) {
        this.id = id;
        this.datePublished = datePublished;
        this.source = source;
        this.mediaTypeRank = mediaTypeRank;
        this.title = title;
        this.url = url;
        this.description = description;
        this.image_url = image_url;
        this.author = author;
        this.date_created = date_created;
        this.likes = likes;
        this.dislikes = dislikes;
        this.shares = shares;
        this.date_created_String = date_created_String;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getMediaTypeRank() {
        return mediaTypeRank;
    }

    public void setMediaTypeRank(String mediaTypeRank) {
        this.mediaTypeRank = mediaTypeRank;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public long getDislikes() {
        return dislikes;
    }

    public void setDislikes(long dislikes) {
        this.dislikes = dislikes;
    }

    public long getShares() {
        return shares;
    }

    public void setShares(long shares) {
        this.shares = shares;
    }

    public String getDate_created_String() {
        return date_created_String;
    }

    public void setDate_created_String(String date_created_String) {
        this.date_created_String = date_created_String;
    }

    @Override
    public String toString() {
        return "CuratedContent{" +
                "id='" + id + '\'' +
                ", datePublished='" + datePublished + '\'' +
                ", source='" + source + '\'' +
                ", mediaTypeRank='" + mediaTypeRank + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", image_url='" + image_url + '\'' +
                ", author='" + author + '\'' +
                ", date_created=" + date_created +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                ", shares=" + shares +
                ", date_created_String='" + date_created_String + '\'' +
                '}';
    }
}
