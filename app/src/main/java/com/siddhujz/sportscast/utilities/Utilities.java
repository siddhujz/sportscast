package com.siddhujz.sportscast.utilities;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by siddhartha on 11/3/15.
 */
public class Utilities {

    public static String normalize(String in) {
        return in.toLowerCase().replaceAll("[-,!'\" ]", "");
    }

    public static String getDomainName(String url) throws URISyntaxException {
        if(!url.startsWith("http") && !url.startsWith("https")){
            url = "http://" + url;
        }
        if (url.equals("No valid link given")) {
            return "unknown";
        }
        URI uri = new URI(url);
        String domain = uri.getHost();
        if(domain == null) {
            if(url.contains("blogger")) {
                System.out.println("Couldn't fetch the domain through code automation. Since the URL contains blogger, domain is being hardcoded to blooger.com");
                return "blogger.com";
            }
            System.out.println("NULL domain detected in URI :" + uri);
            return "unknown";
        }
        return  domain.startsWith("www.") ? domain.substring(4) : domain;
    }
}
