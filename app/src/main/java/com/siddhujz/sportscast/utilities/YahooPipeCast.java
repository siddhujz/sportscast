package com.siddhujz.sportscast.utilities;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.siddhujz.sportscast.Constants;
import com.siddhujz.sportscast.models.CuratedContent;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class YahooPipeCast {

    private static final String TAG = "TitlesFragment";

    public static List<CuratedContent> getYahooPipeCast(String pipeUrl) {
        //Execution start time
        long exeStartTime = new Date().getTime();
        Log.i(TAG, "YahooPipeCast:entered getYahooPipeCast()");

        //Constants used
        int noOfItemsNeeded = 50;
        int descriptionPreLimit = 250;
        int descriptionPostLimit = 350;
        String defaultDescription = "This link is the new trending and the one of the most visited, looked at by the world ";
        String mediaTypeRankYahooPipeDefault = "blog";
        String defaultImageURL = Constants.DEFAULT_YAHOO_PIPE_IMAGE_URL;   //Used in case a blog doesn't have any image in it
        String yahooPipeURL = Constants.DEFAULT_YAHOO_PIPE_URL;
        if (pipeUrl != null && !pipeUrl.isEmpty()) {
            yahooPipeURL = pipeUrl;
        }
        //Objects used for data transimission
        List<CuratedContent> curatedContentList = new ArrayList<CuratedContent>();

        try {
            Log.i(TAG, "YahooPipeCast:inside getYahooPipeCast() - Sending a HTTP GET request");
            URL urlObject = new URL(yahooPipeURL);     //Sending a HTTP GET request
            Log.i(TAG, "YahooPipeCast:inside getYahooPipeCast() - Opening HttpURLConnection");
            HttpURLConnection clientRequest = (HttpURLConnection) urlObject.openConnection();
            Log.i(TAG, "YahooPipeCast:inside getYahooPipeCast() - setting RequestMethod to GET");
            clientRequest.setRequestMethod("GET");
            Log.i(TAG, "YahooPipeCast:inside getYahooPipeCast() - getting ResponseCode");
            int responseCode = clientRequest.getResponseCode();
            //Interested variables used for saving extracted data
            String imageURL = "";
            String moreLink;
            String description;
            String title,pubDate,author = "";
            String sourceDomain;
            String parsedDescription;

            //External Helper Objects
            SimpleDateFormat destFormatter = new SimpleDateFormat("dd/MM/yyyy");  //formats to our followed nomenclature

            System.out.println("\nResponse HTTP code is :" + responseCode);
            if (responseCode == 200) {
                InputStreamReader inputStreamReader = new InputStreamReader(clientRequest.getInputStream());
                BufferedReader isReader = new BufferedReader(inputStreamReader);
                JsonParser parserObj = new JsonParser();
                StringBuilder jsonSB = new StringBuilder();
                String line;
                while ( (line = isReader.readLine()) != null) {
                    jsonSB.append(line + "\n");
                }
                isReader.close();
                System.out.println("Current1 Execution time is " + (new Date().getTime() - exeStartTime));
                JsonElement responseJsonElement = parserObj.parse(jsonSB.toString());
                //JsonElement responseJsonElement = parserObj.parse(isReader);
                JsonObject responseJsonObject = responseJsonElement.getAsJsonObject();

                JsonArray itemsJsonArray=responseJsonObject.getAsJsonObject("value").getAsJsonArray("items");
                JsonObject tempJsonObject;
                int jsonObjectsAvailable = itemsJsonArray.size();

                //Total Execution time = Execution start time - Execution end time
                long curExeEndTime = new Date().getTime();
                long curExeTime = curExeEndTime - exeStartTime;
                System.out.println("Current Execution time is " + curExeTime);

                for(int i = 0;(i < noOfItemsNeeded) && (i < jsonObjectsAvailable); i++)
                {
                    int missedCount = 0;  //Has a count how many required fileds by curated content are missing in the source given
                    JsonElement tempJsonElement = itemsJsonArray.get(i);
                    tempJsonObject = tempJsonElement.getAsJsonObject();
                    if(tempJsonObject.get("title")!= null) {
                        title = tempJsonObject.get("title").toString();
                    } else if (tempJsonObject.get("y:title") != null){
                        title = tempJsonObject.get("y:title").toString();
                    } else {
                        missedCount++;
                        title = "Interior design article";
                    }
                    title = title.replace("\\\"", "");

                    if(tempJsonObject.get("pubDate") != null) {
                        pubDate = tempJsonObject.get("pubDate").getAsString();
                        //EE, dd MMM yyyy HH:mm:ss z  = Sat Jun 01 12:53:10 IST 2013
                        SimpleDateFormat formatter = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss");
                        try {
                            Date parsedDate = formatter.parse(pubDate);
                            pubDate = destFormatter.format(parsedDate);
                        } catch (ParseException parseexception) {  //If date sent is notin parseable format then set today's date
                            Date today = new Date();
                            pubDate = destFormatter.format(today);
                        }
                    } else {    // If the published date given by them is NULL, assign to todays date
                        Date today = new Date();
                        pubDate = destFormatter.format(today);
                    }

                    if (tempJsonObject.get("link") != null) {
                        moreLink = tempJsonObject.get("link").getAsString();
                    } else if (tempJsonObject.get("feedburner:origlink") != null){
                        moreLink = tempJsonObject.get("feedburner:origlink").getAsString();
                    } else if(tempJsonObject.get("guid")!= null && tempJsonObject.get("guid").getAsJsonObject().get("content") != null) {
                        moreLink = tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString();
                    } else {
                        moreLink = "Novalidlinkgiven";   //This string is hardcoded and used  in Utilities class
                        missedCount++;
                    }

                    String sourceHelper = "";
                    if (tempJsonObject.has("comments")) {
                        sourceHelper = tempJsonObject.get("comments").getAsString();
                    } else if (tempJsonObject.get("link") != null) {
                        sourceHelper = tempJsonObject.get("link").getAsString();
                    } else if (tempJsonObject.get("feedburner:origlink") != null){
                        sourceHelper = tempJsonObject.get("feedburner:origlink").getAsString();
                    } else if(tempJsonObject.get("guid")!= null && tempJsonObject.get("guid").getAsJsonObject().get("content") != null && !(tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString().contains("blogger.com")) ){
                        sourceHelper = tempJsonObject.get("guid").getAsJsonObject().get("content").getAsString();
                    } else {
                        sourceHelper = "Novalidlinkgiven";   //This string is hardcoded and used  in Utilities class
                        missedCount++;
                    }
                    //System.out.println(curatedContentList.size() + " - Source link helper is :" + sourceHelper);

                    if(tempJsonObject.has("description") && !tempJsonObject.get("description").isJsonNull()) {
                        description = tempJsonObject.get("description").getAsString();
                    } else if (tempJsonObject.get("content:encoded") != null){
                        if (tempJsonObject.get("content:encoded").isJsonPrimitive()) {
                            description = tempJsonObject.get("content:encoded").getAsString();
                        } else {
                            description = tempJsonObject.get("content:encoded").getAsJsonObject().get("content").getAsString();
                        }
                    } else {
                        description = defaultDescription;
                        missedCount++;
                    }

                    if(tempJsonObject.get("author")!= null)
                    {
                        JsonObject authortemp = null;
                        if(tempJsonObject.get("author").isJsonObject()) {
                            authortemp = tempJsonObject.get("author").getAsJsonObject();
                            author = authortemp.get("name").getAsString();
                        } else {
                            author = tempJsonObject.get("author").toString();
                        }
                    } else if (tempJsonObject.get("dc:creator") != null)
                    {
                        if (tempJsonObject.get("dc:creator").isJsonPrimitive()) {
                            author = tempJsonObject.get("dc:creator").getAsString();
                        } else {
                            author = tempJsonObject.get("dc:creator").getAsJsonObject().get("content").getAsString();
                        }
                    } else {
                        author = "Author unknown";
                        missedCount++;
                    }

                    try {
                        sourceDomain = Utilities.getDomainName(sourceHelper);
                    } catch (URISyntaxException uriSyntaxException) {
                        //System.out.println("Exception raised while getting the source domain of a URL");
                        uriSyntaxException.printStackTrace();
                        continue;
                    }
                    Document doc = Jsoup.parse(description);
                    parsedDescription = doc.text();             //Jsoup.parse(description).text();

                    Elements images = doc.select("img[src~=(?i)\\.(png|jpe?g|gif)]"); //(description,"img[src~=(?i)\\.(png|jpe?g|gif)]");
                    //System.out.println(curatedContentList.size() + " - Title : " + title);
                    if (images.size() == 0 || images.get(0).attr("src").contains("rss.feedsportal")) {
                        imageURL = defaultImageURL;
                        missedCount++;
                    } else if(images.size() > 0) {
                        imageURL = images.get(0).attr("src");
                    }
                    //If more than 3 required fields are not given in the source- skip the source element
                    if (missedCount >= 3) {
                        //System.out.println("Skipping an element due to inadequate fields");
                        continue;
                    }

                    int endIndex;  //Stores the end Index till Descriptionm has to be stored
                    if(parsedDescription.length() > descriptionPreLimit) {
                        String tailString = (parsedDescription.length() >= descriptionPostLimit)?parsedDescription.substring(descriptionPreLimit,descriptionPostLimit):parsedDescription.substring(descriptionPreLimit);
                        if(tailString.contains(".")) {
                            endIndex = tailString.lastIndexOf(".");
                            endIndex += descriptionPreLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex);
                        }
                        else if (tailString.contains("...")) {
                            endIndex = tailString.indexOf("...");
                            endIndex += descriptionPreLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex) + "...";
                        }
                        else {
                            endIndex = (parsedDescription.length() <= descriptionPostLimit)?parsedDescription.length():descriptionPostLimit;
                            parsedDescription = parsedDescription.substring(0,endIndex) + "...";
                            //System.out.println("The string length for" + title + " is more than prelimit and doesn't have ends : " + parsedDescription.length());
                        }
                    }

                    CuratedContent curatedContentObj = new CuratedContent();
                    curatedContentObj.setAuthor(author);
                    //Date at which the blog is created
                    curatedContentObj.setDate_created(destFormatter.parse(pubDate).getTime()); //String --> Date obj --> Long (milliseconds)
                    curatedContentObj.setDate_created_String(pubDate);
                    //The below is the date at which we have published onto our platform
                    curatedContentObj.setDatePublished(destFormatter.format(new Date()));
                    curatedContentObj.setDescription(parsedDescription);
                    curatedContentObj.setId(Utilities.normalize(moreLink));
                    curatedContentObj.setImage_url(imageURL);
                    curatedContentObj.setMediaTypeRank(mediaTypeRankYahooPipeDefault);
                    curatedContentObj.setSource(sourceDomain);
                    curatedContentObj.setUrl(moreLink);
                    curatedContentObj.setTitle(title);

                    curatedContentList.add(curatedContentObj);
               }
            }
            //TODO - Insert into database here.
            Log.i(TAG, "YahooPipeCast:inside getYahooPipeCast() - curatedContentList.size() = " + curatedContentList.size());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        //Total Execution time = Execution start time - Execution end time
        long exeEndTime = new Date().getTime();
        long exeTime = exeEndTime - exeStartTime;
        Log.i(TAG, "YahooPipeCast:exiting getYahooPipeCast() - Execution time is " + exeTime);
        return curatedContentList;
    }
}